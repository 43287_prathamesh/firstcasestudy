#include <stdio.h>
#include "library.h"
#include <string.h>

//user functions
void user_accept(user_t *u)
{
//printf("Id is");
//  scanf("%d",&u->id);
u->id=get_next_user_id(); 
printf("Name is");
scanf("%s",u->name);
printf("Email is");
scanf("%s",u->email);
printf("Phoneno is");
scanf("%s",u->phone);
printf("Password is");
scanf("%s",u->password);

strcpy(u->role,ROLE_MEMBER);

}


void user_display(user_t *u) 
{

printf("ID is: %d/n Name is: %s/n Email is: %s/n Phone is:%s/n Role is: %s/n",u->id,u->name,u->email,u->phone,u->role);

}

void book_accept(book_t *b)

{

//printf("Id is");
//scanf("/n%d",&b->id);
printf("Name is");
scanf("%s",b->name);
printf("Author is");
scanf("%s",b->author);
printf("Subject is");
scanf("%s",b->subject);
printf("Price is");
scanf("%lf",&b->price);
printf("Isbn is");
scanf("%d",&b->isbn);


}
void book_display(book_t *b)
 {
	 printf("%d %s %s %s %.21f %d",b->id,b->name,b->author,b->subject,b->price,b->isbn);
	 	//printf(" Id :%d,Name : %s,Author: %s, Subject %s,price : %.2lf, isbn: %s\n", b->id, b->name, b->author, b->subject, b->price, b->isbn);
}


void bookcopy_accept(bookcopy_t *c)
{ 
  //  printf("Id is");
  //  scanf("%d",&c->id);
    printf("Book id is");
    scanf("%d",&c->bookid);
printf("Rack ");
scanf("%d",&c->rack);
printf("status");
strcpy(c->status,STATUS_AVAIL);

}

void bookcopy_display(bookcopy_t *c)
 {
	printf("%d, %d, %d, %s\n", c->id, c->bookid, c->rack, c->status);
}
void payment_accept(payment_t *p)

{

//printf("Id is");
//scanf("%d",&p->id);
printf("memberid is");
scanf("%d",&p->memberid);
//printf("Type (fees/fine) is");
//scanf("%s",p->type);
strcpy(p->type, PAY_TYPE_FEES);
printf("Amount is");
scanf("%lf",&p->amount);
          //doubt h 

p->tx_time = date_current();
	//if(strcmp(p->type, PAY_TYPE_FEES) == 0)
		p->next_pay_duedate = date_add(p->tx_time, MEMBERSHIP_MONTH_DAYS);
//	else
//		memset(&p->next_pay_duedate, 0, sizeof(date_t));


}

void payment_display(payment_t *p) {
	printf("payment: %d, member: %d, type :%s, amount: %.2lf\n", p->id, p->memberid, p->type, p->amount);
	printf("payment ");
	date_print(&p->tx_time);
	printf("payment due");
	date_print(&p->next_pay_duedate);
}

void issuerecord_accept(issuerecord_t *r) //pending for definition
{
// printf("Id is");
// scanf("%d",&r->id);
printf("copyid is");
scanf("%d",&r->copyid);
printf("Member Id is");
scanf("%d",&r->memberid);
printf("issue_date");
date_accept(&r->issue_date);              //doubt

r->return_duedate=date_add(r->issue_date,BOOK_RETURN_DAYS);     //doubt
memset(&r->return_duedate,0,sizeof(date_t));   //doubt
r->fine_amount=0.0;   //doubt





}


void issuerecord_display(issuerecord_t *r) {
	printf("issue record: %d, copy: %d, member: %d, find: %.2lf\n", r->id, r->copyid, r->memberid, r->fine_amount);
	printf("issue ");
	date_print(&r->issue_date);
	printf("return due ");
	date_print(&r->return_duedate);
	printf("return ");
	date_print(&r->return_date);

} 
void user_add(user_t *u)
{
    //open the file for appending the file  
   FILE *fp ;
   fp= fopen(USER_DB,"ab");
   if (fp==NULL)
   {
   perror("failed to open users file ");//perror satands for print error
        return;
   } 


    //write user data in to the file
    //close the file
  fwrite(u, sizeof(user_t),1,fp); // 1 is how many users
   printf("user added in to the file.\n ");

  //close the file 
  fclose(fp);

}
int user_find_by_email(user_t *u,char email[])  
{
  FILE *fp;
  int found=0;
 fp= fopen(USER_DB,"rb");
   if (fp==NULL)
   {
   perror("failed to open users file ");//perror satands for print error
        return found; ;
   } 


//open the file for readin the data
while(fread(u,sizeof(user_t),1,fp)>0)
{
//read all users one by one
//if user ka email matches    ,return 1 

if (strcmp(u->email,email)==0)
{

  found=1;
  break;
}
}
//close the file, 
fclose(fp);
//return 0 if user is not found 
return found;
}

int get_next_user_id()
{
  FILE *fp;
int max=0;
user_t u;
int size =sizeof(user_t);
  //open the file
  fp=fopen(USER_DB,"rb");
if (fp==NULL)
{
  return max+1;

}
//change file position to last record
fseek(fp,-size,SEEK_END);
//read the record from the file
if(fread(&u,size,1,fp)>0)
{
  max=u.id;
}
//if read is succesfull get maximum (gets its id) id 
//close the file
fclose(fp);

//return max +1;  

return max+1;

}


int get_next_book_id()
{

FILE *fp;
	int max = 0;
	int size = sizeof(book_t);
	book_t u;
	// open the file
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max +1 ;


}


void book_find_by_name(char name[]) {
	FILE *fp;
	int found = 0;
	book_t b;
	// open the file for reading the data
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL) {
		perror("failed to open books file");
		return;
	}
	// read all books one by one
	while(fread(&b, sizeof(book_t), 1, fp) > 0) {
		// if book name is matching partially, found 1
		if(strstr(b.name, name) != NULL) {
			found = 1;
			book_display(&b);
		}
	}
	// close file
	fclose(fp);
	if(!found)
		printf("No such book found.\n");
}


int get_next_bookcopy_id() 
{
	FILE *fp;
	int max = 0;
	int size = sizeof(issuerecord_t);
	issuerecord_t u;
	// open the file
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp == NULL)	
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}


int get_next_issuerecord_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(issuerecord_t);
	issuerecord_t u;
	// open the file
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}


int get_next_payment_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(payment_t);
	payment_t u;
	// open the file
	fp = fopen(PAYMENT_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}
