#ifndef _LIBRARY_H
#define _LIBRARY_H

#include "date.h"

#define USER_DB  "user_db"
#define BOOK_DB		"books.db"
#define BOOKCOPY_DB	"bookcopies.db"
#define ISSUERECORD_DB	"issuerecord.db"
#define PAYMENT_DB	"payment.db"


#define ROLE_OWNER 		"owner"
#define ROLE_LIBRARIAN 	"librarian"
#define ROLE_MEMBER 	"member"

#define STATUS_AVAIL	"available"
#define STATUS_ISSUED	"issued"

#define PAY_TYPE_FEES	"fees"
#define PAY_TYPE_FINE	"fine"

#define FINE_PER_DAY			5
#define BOOK_RETURN_DAYS		7
#define MEMBERSHIP_MONTH_DAYS	30

#define EMAIL_OWNER    "jayeshadi@gmail.com"


typedef struct user

{
   int id;
   char name[100];
   char email[50];
   char phone[15];
   char password[50];
   char role[50];

}user_t;

typedef struct book

{
   
    int id;
   char name[100];
   char author[50];
   char subject[50];
   double price;
   int isbn;                 //uthe char h

   
}book_t;
typedef struct issuerecord

{
   int id;
   int copyid;
   int memberid;
   date_t issue_date;
   date_t return_duedate;
   date_t return_date;
   double fine_amount;

}issuerecord_t;

typedef struct bookcopy
{
int id;
int bookid;
int  rack;
char status[10];


}bookcopy_t;

typedef struct payment {
	int id;
	int memberid;
	double amount;
	char type[10];
	date_t tx_time;
	date_t next_pay_duedate;
}payment_t;

//user defined functin 

void user_accept(user_t *u);
void user_display(user_t *u); 

//book fuctions
void book_accept(book_t *b);
void book_display(book_t *b); 


 void bookcopy_accept(bookcopy_t *c);
void bookcopy_display(bookcopy_t *c);


void payment_accept(payment_t *p);
void payment_display(payment_t *p);

void issuerecord_accept(issuerecord_t *r);
void issuerecord_display(issuerecord_t *r);




//owner functions

void owner_area(user_t *u);

void appoint_librarian();


//librarian functions
void librarian_area(user_t *u);

void add_book();

void add_member();

void book_edit_by_id();

void bookcopy_add();

void bookcopy_checkavail_details();

void bookcopy_issue();

void bookcopy_changestatus(int bookcopy_id, char status[]);

void display_issued_bookcopies(int member_id);
void bookcopy_return();

void fees_payment_add();
void payment_history(int memberid);

int is_paid_member(int memberid);

void fine_payment_add(int memberid, double fine_amount);


//member fun

void member_area(user_t *u);

int get_next_bookcopy_id();

int get_next_issuerecord_id();

int get_next_payment_id();

void bookcopy_checkavailibilty();
//common fun
void sign_in();


void sign_up();

void edit_profile(user_t *u);
 
void change_password(user_t u);

int get_next_user_id();

int get_next_book_id();

int get_next_issuerecord_id();

void add_user(user_t *u);       

int user_find_by_email(user_t *u,char email[]);    // explantion if user mil gaya ,then we will return 1 else 0

void book_find_by_name(char name[]);

//test function


#endif
