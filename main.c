#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "library.h"
#include "date.h"

void date_tester() {
	date_t d1 = {1, 1, 2000}, d2 = {31, 12, 2000};
	date_t d = {1, 1, 2000};
	date_t r = date_add(d, 366);
	date_print(&r);
	int diff = date_cmp(d1, d2);
	printf("date diff: %d\n", diff);
}

void tester() {
	user_t u;
	book_t b;
	user_accept(&u);
	user_display(&u);
	book_accept(&b);
	book_display(&b);
}


void sign_in()
{
	char email[30], password[10];
	user_t u;
	int invalid_user=0;

	//i/p email n password from input 
	printf("email");
	scanf("%s",email);
	printf("password");
	scanf("%s",password);

//find the user in the users fileby  email
if(user_find_by_email(&u, email)==1)
{
	if (strcmp(password,u.password)==0)
	{
//special sceario for owner 
if (strcmp(email,EMAIL_OWNER) ==0)
strcpy(u.role,ROLE_OWNER);
		if (strcmp(u.role,ROLE_OWNER)==0)
			owner_area(&u);
		else if (strcmp(u.role,ROLE_LIBRARIAN)==0)
			librarian_area(&u);
			else if (strcmp(u.role,ROLE_MEMBER)==0)
			member_area(&u);
		else
		{
			invalid_user=1;  
		}
		 

	}
	else
	{
		invalid_user=1;
	}
	
// check input password is correct.
//if correct , call user_area() based on its role

}
else
{
	invalid_user=1;
}

if (invalid_user)
{
	printf("wrong email,password or role");

}

}



void sign_up()
{


//input user details from the user
user_t u;
user_accept(&u);           
  	//write user details in to the user file 
 user_add(&u);
		//basically create database file

}

int main()
{
	//char name[80];
	//book_t b;
	//printf("enter name");
	//scanf("%s",name);
	//book_accept(&b);
	 //book_find_by_name(name);
	//add_book();

  //int id=get_next_user_id();
  // printf("new id :%d",id);
// printf("Hello librarian\n"); 

 // tester();
 
 int choice;
 do{
 		printf("\n\n 0. Exit \n 1.Sign in \n 2.Sign up \n Enter your choice ");
 		scanf("%d",&choice);
 

 switch (choice)
 {

	 case 1: //sign in
	 sign_in();
	 break;
	 case 2:  //sign up

	 sign_up();
	 break;

 }

}while(choice !=0); 


  return 0;
}